$(document).ready(function () {
    $("#modal-show").click(function () {
      $(".modal").fadeIn();
    });
    $("#close").click(function () {
      $(".modal").fadeOut();
    });
    $('.owl-carousel').owlCarousel({
      center: true,
      video:true,
      lazyLoad:false,
      items:2,
      dots:true,
      loop:true,
      margin:10,
    });
  });